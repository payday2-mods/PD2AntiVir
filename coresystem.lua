
local overrides = {
	file = {
		forbid = {
			CreateDirectory = false, -- linux only
			MoveDirectory = false, -- windows only
			RemoveDirectory = false
		}
	},
	io = {
		forbid = {
			open = true,
			popen = true,
			save_as_json = false,
			remove_directory_and_files = false,
			write = false
		}
	},
	SystemFS = {
		forbid = {
			make_dir = false,
			delete_file = false,
			open = true,
			copy_file = false,
			copy_files_async = false,
			rename_file = false
		},
		pass = {
			"exists",
			"list",
			"close",
			"parse_xml",
			"is_dir"
		}
	}
}

--[[
local allow = {
	{
		mod = "Enhanced Hitmarkers",
		masks = {
			"assets/mod_overrides/Enhanced Hitmarkers/*.texture"
		}
	},
	{
		mod = "Newbies go back to overkill",
		masks = {
			"blacklist.ini",
			"kicklist.ini"
		}
	},
	{
		mod = "BeardLib",
		masks = {
			"mods/*/config.xml",
			"mods/*/Config.xml"
		}
	}
}
]]

local charset = {}
for i = 65,  90 do table.insert(charset, string.char(i)) end
for i = 97, 122 do table.insert(charset, string.char(i)) end
local function rndStr(length)
	math.randomseed(os.time())
	if length > 0 then
		return rndStr(length - 1) .. charset[math.random(1, #charset)]
	else
		return ""
	end
end

local function processPath(source)
	if source:sub(1, 2) == "--" then
		return "@" .. source:sub(3, source:find("\n") - 1)
	elseif source:sub(1, 1) == "@" then
		return source
	end
	return nil
end

local fc = rndStr(32)
log("[PD2AntiVir] [INFO] Building function cache with the randomly generated name: '" .. fc .. "'")
_G[fc] = {}
for class, override in pairs(overrides) do
	if _G[class] then
		local ud = type(_G[class]) == "userdata"
		if ud then
			_G[fc][class] = _G[class]
			_G[class] = {}
			for _, func in pairs(override.pass) do
				_G[class][func] = function(_, ...)
					return _G[fc][class][func](_G[fc][class], ...)
				end
			end
		else
			_G[fc][class] = {}
		end
		for func, defret in pairs(override.forbid) do
			if not ud and _G[class][func] then
				_G[fc][class][func] = _G[class][func]
			end
			if _G[fc][class][func] then
				log("[PD2AntiVir] [INFO] Hooking function '" .. class .. "." .. func .. "'...")
				_G[class][func] = function(...)
					local cancel
					local params = {...}
					if ud then
						table.remove(params, 1)
					end
					local info = debug.getinfo(2)
					local source = processPath(info.source)
					if source then
						if source:sub(2, 6) ~= "mods/" then
							cancel = "suspicious source path"
						else
							local utilsiopath = Application:nice_path("mods/base/req/utils/UtilsIO.lua")
							local sourcepath = Application:nice_path(source:sub(2))
							if Idstring(sourcepath) == Idstring(utilsiopath) then
								source = processPath(debug.getinfo(3).source)
								sourcepath = Application:nice_path(source:sub(2))
							end
							local modname = source:sub(7, source:sub(7):find("/") + 5)
							local modpath = Application:nice_path("mods/" .. modname)
							local savespath = Application:nice_path("mods/saves")
							local actionpath = Application:nice_path(params[1])
							if Idstring(actionpath:sub(1, savespath:len())) ~= Idstring(savespath) and Idstring(actionpath:sub(1, modpath:len())) ~= Idstring(modpath) then
								if Idstring(modname) ~= Idstring("base") then
									if ((class == "io" or class == "SystemFS") and func == "open" and (params[2] == "rb" or params[2] == "r")) then
										-- opening as read only is okay
									else
										cancel = "suspicious action path"
									end
								end
							end
						end
					end
					if cancel then
						local caller = "<UNKNOWN CALLER>"
						if info.what == "Lua" and info.name then
							caller = "'" .. info.name .. "' of '" .. source .. "'"
						elseif info.what == "main" or info.what == "Lua" then
							caller = "''" .. source .. "'"
						elseif info.what == "C" then
							caller = "A C-function"
							log("===========================================================================================")
							log("UH WE GOT A C CALL HERE, LETS DEBUG IT:")
							PrintTable(info)
							log("===========================================================================================")
						end
						log("=====================================[PD2AntiVir]==========================================")
						log(caller .. " tried to call '" .. class .. "." .. func .. "'.")
						log("Parameters:")
						PrintTable(params)
						log("Cancel Reason: ".. cancel)
						log("===========================================================================================")
					end
						--return (defret and nil or false)
					--else
						return _G[fc][class][func](unpack(ud and {_G[fc][class], unpack(params)} or params))
					--end
				end
			else
				log("[PD2AntiVir] [WARNING] Could not hook onto function: '" .. class .. "." .. func .. "'. It does not exist.")
			end
		end
	else
		log("[PD2AntiVir] [WARNING] Could not hook onto class: '" .. class .. "'. It does not exist.")
	end
end
log("[PD2AntiVir] [INFO] Function cache built successfuly.")
